package com.viparking.viparking.util;

/**
 * Created by ricky on 8/11/2016.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
