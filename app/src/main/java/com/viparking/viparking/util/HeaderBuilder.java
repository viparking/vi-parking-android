package com.viparking.viparking.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by Ferico on 7/5/16.
 */
public class HeaderBuilder {
    public static String getBearer()
    {
        String combined = Util.GET_ACCESS_TOKEN()+":"+Util.GET_USER_ID()+":"+Util.API_KEY;
        String base64 = "";
        try {
            byte[] data = combined.getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
            base64 = base64.replace("\n","");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return base64;
    }
}
