package com.viparking.viparking.util;

import android.content.Context;

import com.viparking.viparking.R;

/**
 * Created by Ferico on 7/5/16.
 */
public final class UrlHelper {

    private static UrlHelper sInstance;
    public final String baseUrl;

    public static UrlHelper getInstance() {
        return sInstance;
    }

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new UrlHelper(context);
        }
    }

    private UrlHelper(Context context) {
        baseUrl = context.getString(R.string.parking_host);
    }

    public String getUrl(String path) {
        return baseUrl + path;
    }
}