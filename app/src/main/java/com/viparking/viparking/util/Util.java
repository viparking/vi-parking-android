package com.viparking.viparking.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;

import com.viparking.viparking.OnBoardingActivity;
import com.viparking.viparking.R;
import com.viparking.viparking.model.User;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.WsManager;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ferico on 7/5/16.
 */
public class Util {

    private static Context context;
    private static Handler saverHandler;
    private static HandlerThread handlerThread;

    public static final String SHARED_PREFERENCES_NAME = "id.amigos.viparkingregistration.SharedPreferences";
    public static final int SPLASH_SCREEN_DURATION = 2000;

    public static final String API_KEY = "263b7f33fdfb189bb5bac3a6ba021637";
    private static final String PREFIX = "com.odeo.odeo.";
    public static final String KEY_USER_ID = PREFIX + "userID";
    public static final String KEY_ACCESS_TOKEN = PREFIX + "accessToken";
    public static final String KEY_USER_NAME = PREFIX + "name";
    private static final String BASE_URL = "http://dev.wintechindo.com/API/";
    public static final String LOGIN_URL = BASE_URL + "user/login";
    public static final String MY_VEHICLE_URL = BASE_URL + "vehicle/me";
    public static final String MY_HISTORY_URL = BASE_URL + "vehicle/me/{vehicleID}/parking";
    public static final String CHANGE_PASSWORD_URL = BASE_URL + "me/changePassword";
    public static final String SEND_EMAIL_HISTORY = BASE_URL + "vehicle/me/{vehicleID}/email";
    public static final String DOWNLOAD_PDF_HISTORY = BASE_URL + "vehicle/user/{vehicleID}/pdf";

    public static final String FILE_PATH = "/ViParking Report/";
    static {
        handlerThread = new HandlerThread("SaverHandlerThread");
        handlerThread.start();
        saverHandler = new Handler(handlerThread.getLooper());
    }

    public static void setContext(Context context) {
        // Get Application Context instead of Activity Context to prevent activity leak
        if (Util.context == null)
            Util.context = context.getApplicationContext();
    }

    public static HandlerThread getHandlerThread() {
        return handlerThread;
    }

    public static void SET_USER_ID(String userID) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_USER_ID, userID).apply();
    }

    public static String GET_USER_ID() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID, "");
    }

    public static void SET_ACCESS_TOKEN(String userID) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_ACCESS_TOKEN, userID).apply();
    }

    public static String GET_ACCESS_TOKEN() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, "");
    }

    public static String GET_USER_NAME() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_NAME, "");
    }

    public static void SET_USER_NAME(String name) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_USER_NAME, name).apply();
    }

    public static void logOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
        ParkingWS.clearQueue();
        WsManager.getInstance().clearAll();
    }

    public static void forceLogOut() {
        logOut();
        Intent reloginIntent = new Intent(context, OnBoardingActivity.class);
        reloginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(reloginIntent);
    }


    public static String formatMoney(int price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMANY);
        return (context.getString(R.string.IDR) +String.valueOf(numberFormat.format(price)));
    }

    public static String CountDuration(String inTime,String outTime) throws ParseException {

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date startDate = simpleDateFormat.parse(inTime);
        Date endDate = simpleDateFormat.parse(outTime);
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        String duration = "";
        if(elapsedDays == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedDays) + "days ";
        }

        if(elapsedHours == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedHours) + "hours ";
        }

        if(elapsedMinutes == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedMinutes) + "mins ";
        }

        if(elapsedSeconds == 0){
            duration += "";
        }else{
            duration += String.valueOf(elapsedSeconds) + "secs ";
        }
        return duration;
    }
}