package com.viparking.viparking.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ferico on 7/29/16.
 */
public class SimpleViewHolder extends RecyclerView.ViewHolder {

    public SimpleViewHolder(View itemView) {
        super(itemView);
    }
}
