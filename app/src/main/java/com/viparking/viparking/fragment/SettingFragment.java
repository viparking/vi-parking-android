package com.viparking.viparking.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.viparking.viparking.R;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.model.webservice.WsResponse;
import com.viparking.viparking.util.Util;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.WsListener;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ferico on 8/11/16.
 */
public class SettingFragment extends Fragment implements View.OnClickListener, WsListener{

    private static SettingFragment instance;

    private EditText txtOldPassword, txtNewPassword, txtNewPasswordConfirm;
    private ProgressDialog mProgressDialog;

    public static SettingFragment getInstance(){
        if(instance == null)
            instance = new SettingFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        txtOldPassword = (EditText) rootView.findViewById(R.id.txtOldPassword);
        txtNewPassword = (EditText) rootView.findViewById(R.id.txtNewPassword);
        txtNewPasswordConfirm = (EditText) rootView.findViewById(R.id.txtNewPasswordConfirm);

        Button btnLogout = (Button) rootView.findViewById(R.id.btnLogout);
        Button btnOK = (Button) rootView.findViewById(R.id.btnOK);

        btnLogout.setOnClickListener(this);
        btnOK.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnOK)
        {
            if(validateInput())
            {
                ParkingWS ws = new ParkingWS(getContext());
                mProgressDialog = ProgressDialog.show(getContext(), getString(R.string.loading),
                        getString(R.string.please_wait), true, false);
                ws.changePassword(this, txtOldPassword.getText().toString(), txtNewPassword.getText().toString());
            }
        }
        else
            Util.forceLogOut();
    }

    private boolean validateInput()
    {
        if(txtNewPassword.getText().length() == 0)
        {
            showErrorMessage(getString(R.string.error_empty_new_password));
            txtNewPassword.requestFocus();
            return false;
        }
        else if(txtNewPasswordConfirm.getText().length() == 0)
        {
            showErrorMessage(getString(R.string.error_empty_new_password_confirm));
            txtNewPasswordConfirm.requestFocus();
            return false;
        }
        else if(txtOldPassword.getText().length() == 0)
        {
            showErrorMessage(getString(R.string.error_empty_old_password));
            txtOldPassword.requestFocus();
            return false;
        }
        else if(!txtNewPasswordConfirm.getText().toString().equals(txtNewPassword.getText().toString()))
        {
            showErrorMessage(getString(R.string.error_password_not_match));
            txtNewPasswordConfirm.setText("");
            txtNewPasswordConfirm.requestFocus();
            return false;
        }
        return true;
    }

    private void showErrorMessage(String error)
    {
        Snackbar snackbar = Snackbar.make(txtNewPassword, error, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onSuccess(WsResponse response) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.information)
                .setMessage(R.string.password_changed)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        mProgressDialog.dismiss();
        txtNewPasswordConfirm.setText("");
        txtNewPassword.setText("");
        txtOldPassword.setText("");
    }

    @Override
    public void onError(String error) {
        mProgressDialog.dismiss();
        showErrorMessage(error);
    }
}
