package com.viparking.viparking.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.viparking.viparking.R;
import com.viparking.viparking.adapter.HistoryListAdapter;
import com.viparking.viparking.model.History;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.model.webservice.HistoryListResponse;
import com.viparking.viparking.model.webservice.WsResponse;
import com.viparking.viparking.util.OnLoadMoreListener;
import com.viparking.viparking.util.Util;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.PdfDownloader;
import com.viparking.viparking.webservice.WsListener;


import java.util.ArrayList;


public class VehicleHistoryFragment extends Fragment  implements WsListener<HistoryListResponse>,
        HistoryListAdapter.HistoryListListener {

    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private static VehicleHistoryFragment instance;

    private Vehicle vehicle;
    private int page = 0;
    private boolean isAppending = false;
    private boolean isNextAvailable = true;
    private HistoryListAdapter adapter;
    private String startDate = "",endDate = "";
    private boolean sendEmail = false;

    @Override
    public void onSuccess(HistoryListResponse response) {

            if (isAppending) {
            ArrayList<History> historyList = adapter.getHistoryList();
            ArrayList<History> responseList = response.getHistoryList();
            if (responseList.size() == 0) {
                isNextAvailable = false;
                return;
            }
            for (int i = 0; i < responseList.size(); i++)
                historyList.add(responseList.get(i));
            adapter.setHistoryList(historyList);
            adapter.notifyDataSetChanged();
            return;
        }
        mProgressDialog.dismiss();
        if(adapter == null) {
            adapter = new HistoryListAdapter(mRecyclerView, response.getHistoryList(), this);
            adapter.setVehicle(vehicle);
            page = 0;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(adapter);
        }
        else
        {
            adapter.setHistoryList(response.getHistoryList());
            adapter.notifyDataSetChanged();
            page = 0;
            isAppending = false;
            isNextAvailable = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        isAppending = false;
        isNextAvailable = true;
    }

    @Override
    public void onError(String error) {
        mProgressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(mRecyclerView, error, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static VehicleHistoryFragment getInstance() {
        if (instance == null)
            instance = new VehicleHistoryFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        adapter = new HistoryListAdapter(mRecyclerView, new ArrayList<History>(), this);
        adapter.setVehicle(vehicle);
        page = 0;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(adapter);

        return rootView;
    }


    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        isAppending = false;
        ParkingWS ws = new ParkingWS(getActivity());
        mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.please_wait), true, false);
        ws.getHistoryList(this, vehicle.getId(),startDate,endDate,0);

    }

    public void setCalendar(String starDate,String endDate){
        this.startDate = starDate;
        this.endDate = endDate;
        setVehicle(vehicle);
    }

    @Override
    public void shouldLoadNextPage() {
        if (!isNextAvailable)
            return;

        ParkingWS ws = new ParkingWS(getActivity());
        //mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.please_wait), true, false);
        isAppending = true;
        ws.getHistoryList(this, vehicle.getId(),startDate,endDate,++page);
    }

    public void sendEmail(String starDate,String endDate) {
        ParkingWS ws = new ParkingWS(getActivity());
        sendEmail = true;
        mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.please_wait), true, false);
        ws.sendEmail(new WsListener() {
            @Override
            public void onSuccess(WsResponse response) {
                mProgressDialog.dismiss();
                Snackbar snackbar = Snackbar.make(mRecyclerView, getString(R.string.success_email), Snackbar.LENGTH_LONG);
                snackbar.show();
            }

            @Override
            public void onError(String error) {
                mProgressDialog.dismiss();
                Snackbar snackbar = Snackbar.make(mRecyclerView, error, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }, vehicle.getId(), startDate, endDate);
    }

    @Override
    public void downloadPdf(String startDate, String endDate) {
        ParkingWS ws = new ParkingWS(getContext());
        mProgressDialog = ProgressDialog.show(getContext(), getString(R.string.loading),
                getString(R.string.please_wait), true, false);
        ws.downloadPdf(new PdfDownloader.PdfDownloaderListerner() {
            @Override
            public void onDownloadCompleted(String fileName) {
                mProgressDialog.dismiss();
                Uri uri = Uri.parse(Environment.getExternalStorageDirectory()
                        + Util.FILE_PATH + fileName);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/pdf");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                Intent chooser = Intent.createChooser(intent, getString(R.string.open_pdf));
                try {
                    startActivity(chooser);
                } catch (ActivityNotFoundException ex) {
                    Snackbar snackbar = Snackbar.make(mRecyclerView, getString(R.string.no_pdf_app), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }, vehicle.getId(), startDate, endDate);
    }
}
