package com.viparking.viparking.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.viparking.viparking.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link OnBoardingFrament#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnBoardingFrament extends Fragment {
    private static final String IMAGE_ID_PARAM = "ViParking.Fragment.ImageId";
    private static final String TITLE_ID_PARAM = "ViParking.Fragment.TitleId";
    private static final String DESCRIPTION_ID_PARAM = "ViParking.Fragment.DescriptionId";

    public OnBoardingFrament() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OnBoardingFrament.
     */
    public static OnBoardingFrament newInstance(int imageResourceId, int titleResourceId, int descriptionResourceId) {
        OnBoardingFrament fragment = new OnBoardingFrament();
        Bundle args = new Bundle();
        args.putInt(IMAGE_ID_PARAM, imageResourceId);
        args.putInt(TITLE_ID_PARAM, titleResourceId);
        args.putInt(DESCRIPTION_ID_PARAM, descriptionResourceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_on_boarding_frament, container, false);
        ImageView imgOnBoarding = (ImageView) v.findViewById(R.id.imgOnBoarding);
        imgOnBoarding.setImageResource(getArguments().getInt(IMAGE_ID_PARAM));
        return v;
    }
}