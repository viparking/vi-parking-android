package com.viparking.viparking.fragment;


import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.viparking.viparking.MainActivity;
import com.viparking.viparking.R;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.model.webservice.VehicleListResponse;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.WsListener;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by ricky on 7/25/2016.
 */
public class CheckSaldoFragment extends Fragment implements View.OnClickListener{

    private static CheckSaldoFragment instance;
    private TextView mtxtSaldo,mtxtPlateNumber;
    private ImageView mImgCarImage;
    private Button mbtnHistory;

    private ViewHistoryListner callback;
    public static CheckSaldoFragment getInstance(ViewHistoryListner callback){
        if(instance == null)
            instance = new CheckSaldoFragment();
        instance.callback = callback;
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_check_saldo, container, false);

        mbtnHistory = (Button)rootView.findViewById(R.id.btnHistory);
        mtxtSaldo = (TextView)rootView.findViewById(R.id.txtSaldo);
        mtxtPlateNumber = (TextView)rootView.findViewById(R.id.lblPlateNumber);
        mImgCarImage = (ImageView)rootView.findViewById(R.id.imgVehicleIcon);
        mbtnHistory.setOnClickListener(this);
        return rootView;
    }

    public void setVehicle(Vehicle vehicle)
    {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMANY);
        mtxtSaldo.setText(getString(R.string.IDR)+String.valueOf(numberFormat.format(vehicle.getBalance())));
        mtxtPlateNumber.setText(vehicle.getPlateNumber());
    }

    @Override
    public void onClick(View v) {
        if(callback != null)
            callback.ViewHistoryListner();
    }

    public interface ViewHistoryListner{
        void ViewHistoryListner();
    }
}
