package com.viparking.viparking.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viparking.viparking.R;
import com.viparking.viparking.adapter.VehicleListAdapter;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.model.webservice.VehicleListResponse;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.WsListener;

/**
 * Created by Ferico on 7/17/16.
 */
public class VehicleListFragment extends Fragment implements WsListener<VehicleListResponse>,
        VehicleListAdapter.VehicleListTouchListener {

    public interface VehicleListListener{
        void onVehicleSelected(Vehicle vehicle);
    }
    private RecyclerView mRecyclerView;
    private VehicleListListener callback;
    private ProgressDialog mProgressDialog;
    private static VehicleListFragment instance;

    public static VehicleListFragment getInstance(VehicleListListener callback){
        if(instance == null) {
            instance = new VehicleListFragment();
        }
        instance.callback = callback;
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_vehicle_list, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        ParkingWS ws = new ParkingWS(getActivity());
        mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.please_wait), true, false);
        ws.getVehicleList(this);
        return rootView;
    }

    @Override
    public void onTouch(Vehicle vehicle) {
        if(callback != null)
        callback.onVehicleSelected(vehicle);
    }

    @Override
    public void onSuccess(VehicleListResponse response) {
        mProgressDialog.dismiss();
        VehicleListAdapter adapter = new VehicleListAdapter(response.getVehicleList(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(adapter);

        if(response.getVehicleList().size() > 0 && callback != null)
            callback.onVehicleSelected(response.getVehicleList().get(0));
    }

    @Override
    public void onError(String error) {
        mProgressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(mRecyclerView, error, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


}
