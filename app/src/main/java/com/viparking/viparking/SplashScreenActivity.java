package com.viparking.viparking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.viparking.viparking.util.Util;
import com.viparking.viparking.webservice.WsManager;

import io.fabric.sdk.android.Fabric;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);

        Util.setContext(SplashScreenActivity.this);
        WsManager.init(this);
        ImageView imgSplashScreen = (ImageView) findViewById(R.id.imgSplashScreen);
        imgSplashScreen.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!Util.GET_USER_ID().equals(""))
                {
                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent = new Intent(SplashScreenActivity.this, OnBoardingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, Util.SPLASH_SCREEN_DURATION);
    }
}
