package com.viparking.viparking.model.webservice;

import com.viparking.viparking.model.User;
import com.viparking.viparking.model.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricky on 7/24/2016.
 */
public class VehicleListResponse extends WsResponse {
    private Vehicle vehicle;
    ArrayList<Vehicle> vehicleList;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        vehicle = new Vehicle();
        vehicleList =  new ArrayList<>();
        try {
            JSONArray data = jsonObject.getJSONArray("data");
            Boolean status = jsonObject.getBoolean("success");

            if(status){

                for(int i = 0 ; i < data.length(); i++)
                {
                    JSONObject obj =  data.getJSONObject(i);
                    vehicle = new Vehicle();
                    vehicle.setId(obj.getString("id"));
                    vehicle.setVehicleTypeId(obj.getInt("vehicle_type_id"));
                    vehicle.setMemberId(obj.getString("member_id"));
                    vehicle.setCardId(obj.getString("card_id"));
                    vehicle.setName(obj.getString("name"));
                    vehicle.setColor(obj.getString("color"));
                    vehicle.setType(obj.getString("vehicleType"));
                    vehicle.setBrand(obj.getString("brand"));
                    vehicle.setAddress(obj.getString("address"));
                    vehicle.setNoStnk(obj.getString("noSTNK"));
                    vehicle.setPlateNumber(obj.getString("plateNumber"));
                    vehicle.setBalance(obj.getInt("balance"));

                    vehicleList.add(vehicle);

                }

            }



        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<Vehicle> getVehicleList(){
         return vehicleList;
    }
}
