package com.viparking.viparking.model;

/**
 * Created by ricky on 8/3/2016.
 */
public class History {

    private String id;
    private int parking_lot_id;
    private String vehicle_id;
    private int payment_type_id;
    private String member_id;
    private int price;
    private String inTime;
    private String outTime;
    private String parkingLotName;
    private String inGate;
    private String outGate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getParking_lot_id() {
        return parking_lot_id;
    }

    public void setParking_lot_id(int parking_lot_id) {
        this.parking_lot_id = parking_lot_id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public int getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(int payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getParkingLotName() {
        return parkingLotName;
    }

    public void setParkingLotName(String parkingLotName) {
        this.parkingLotName = parkingLotName;
    }

    public String getInGate() {
        return inGate;
    }

    public void setInGate(String inGate) {
        this.inGate = inGate;
    }

    public String getOutGate() {
        return outGate;
    }

    public void setOutGate(String outGate) {
        this.outGate = outGate;
    }
}
