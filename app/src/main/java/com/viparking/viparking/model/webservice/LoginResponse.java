package com.viparking.viparking.model.webservice;

import com.viparking.viparking.model.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ferico on 7/5/16.
 */
public class LoginResponse extends WsResponse {
    private User user;
    private String accessToken;
    private boolean isMember = true;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        user = new User();
        try {
            JSONObject data = jsonObject.getJSONObject("data");
            JSONObject userObject = data.getJSONObject("user");

            accessToken = jsonObject.getString("token");
            if(!userObject.has("memberType"))
            {
                isMember = false;
                return;
            }

            user.setId(userObject.getString("id"));
            user.setMemberTypeId(userObject.getInt("member_type_id"));
            user.setUsername(userObject.getString("username"));
            user.setEmail(userObject.getString("email"));
            user.setPhoneNumber(userObject.getString("phoneNumber"));
            user.setPoint(userObject.getInt("point"));
            user.setBanned(userObject.getInt("isBanned") == 1);
            user.setAlamatTinggal(userObject.getString("alamatTinggal"));
            user.setAlamatKtp(userObject.getString("alamatKTP"));
            user.setNoKtp(userObject.getString("noKTP"));
            user.setMemberType(userObject.getString("memberType"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public User getUser() {
        return user;
    }

    public boolean isMember() {
        return isMember;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
