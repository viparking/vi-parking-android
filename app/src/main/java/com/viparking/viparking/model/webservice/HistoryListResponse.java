package com.viparking.viparking.model.webservice;

import com.viparking.viparking.model.History;
import com.viparking.viparking.model.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ricky on 8/3/2016.
 */
public class HistoryListResponse extends WsResponse {
    private History history;
    ArrayList<History> historyList;

    @Override
    protected void parseContent(JSONObject jsonObject) {
        try {
            super.parseContent(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        history = new History();
        historyList = new ArrayList<>();
        try {
            JSONArray data = jsonObject.getJSONArray("data");
            Boolean status = jsonObject.getBoolean("success");
            int total = jsonObject.getInt("total");

            if(status && total > 0){

                for(int i = 0 ; i < data.length(); i++)
                {
                    JSONObject obj =  data.getJSONObject(i);
                    history = new History();
                    history.setId(obj.getString("id"));
                    history.setParking_lot_id(obj.getInt("parking_lot_id"));
                    history.setVehicle_id(obj.getString("vehicle_id"));
                    history.setPayment_type_id(obj.getInt("payment_type_id"));
                    history.setMember_id(obj.getString("member_id"));
                    history.setPrice(obj.getInt("price"));
                    history.setInTime(obj.getString("inTime"));
                    history.setOutTime(obj.getString("outTime"));
                    history.setParkingLotName(obj.getString("parkingLotName"));
                    history.setInGate(obj.getString("inGate"));
                    history.setOutGate(obj.getString("outGate"));
                    historyList.add(history);

                }

            }



        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<History> getHistoryList(){
        return historyList;
    }
}
