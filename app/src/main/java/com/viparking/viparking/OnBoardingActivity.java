package com.viparking.viparking;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.viparking.viparking.adapter.OnBoardingAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class OnBoardingActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ViewPager mViewPager;
    private LinearLayout indicatorContainer;
    private boolean isBeingScrolled;
    private Timer mTimer;
    private static int SLIDING_DELAY_MSEC = 5000;
    private static final int ITEM_LENGTH = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        OnBoardingAdapter adapter = new OnBoardingAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);

        indicatorContainer = (LinearLayout) findViewById(R.id.containerIndicator);

        int childCount = indicatorContainer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = indicatorContainer.getChildAt(i);
            final int finalI = i;
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewPager.setCurrentItem(finalI, true);
                }
            });
        }

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new OnboardScrollTask(), SLIDING_DELAY_MSEC, SLIDING_DELAY_MSEC);

        Button btnSkip = (Button) findViewById(R.id.btnSkip);
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnBoardingActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int childCount = indicatorContainer.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = indicatorContainer.getChildAt(i);
            child.setBackgroundResource(i == position ?
                    R.drawable.circle_indicator_active
                    : R.drawable.circle_indicator_inactive);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        boolean isBeingScrolledCurrent = state != ViewPager.SCROLL_STATE_IDLE;

        if (isBeingScrolledCurrent != isBeingScrolled) {

            if (isBeingScrolledCurrent) {
                mTimer.cancel();
            } else {
                mTimer = new Timer();
                mTimer.scheduleAtFixedRate(new OnboardScrollTask(), SLIDING_DELAY_MSEC, SLIDING_DELAY_MSEC);
            }

            isBeingScrolled = isBeingScrolledCurrent;
        }
    }

    private class OnboardScrollTask extends TimerTask {

        @Override
        public void run() {
            mViewPager.post(viewPagerUpdateRunnable);
        }
    }

    private Runnable viewPagerUpdateRunnable = new Runnable() {

        @Override
        public void run() {
            int currentIndex = mViewPager.getCurrentItem();
            mViewPager.setCurrentItem(currentIndex < ITEM_LENGTH - 1 ? currentIndex + 1 : 0, true);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
    }
}
