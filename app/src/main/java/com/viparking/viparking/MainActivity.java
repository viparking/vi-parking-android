package com.viparking.viparking;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.viparking.viparking.adapter.DashboardPagerAdapter;
import com.viparking.viparking.fragment.CheckSaldoFragment;
import com.viparking.viparking.fragment.VehicleListFragment;
import com.viparking.viparking.model.Vehicle;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,
        VehicleListFragment.VehicleListListener,CheckSaldoFragment.ViewHistoryListner {

    DashboardPagerAdapter adapter;
    ViewPager mViewPager;
    private TabLayout mTabLayout;
    private AppBarLayout mAppBarLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        adapter = new DashboardPagerAdapter(getSupportFragmentManager(), MainActivity.this, this,this);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);
//        mViewPager.addOnPageChangeListener(this);

        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
//        mTabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                super.onTabReselected(tab);
//                getFragment(tab.getPosition()).onTabReselected();
//                mAppBarLayout.setExpanded(true, true);
//
//            }
//        });

        setupTabLayout();

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
    }

//    private BaseFragment getFragment(int position) {
//        String tag = "android:switcher:" + mViewPager.getId() + ":" + position;
//        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
//    }

    private void setupTabLayout() {
        mTabLayout.getTabAt(0).setIcon(R.drawable.cek_saldo_icon);
        mTabLayout.getTabAt(1).setIcon(R.drawable.change_vehicle_icon);
        mTabLayout.getTabAt(2).setIcon(R.drawable.history_icon);
        mTabLayout.getTabAt(3).setIcon(R.drawable.setting_icon);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setupTabLayout();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onVehicleSelected(Vehicle vehicle) {
        mViewPager.setCurrentItem(0, true);
        adapter.setVehicle(vehicle);
        adapter.setHistoryList(vehicle);
    }

    @Override
    public void ViewHistoryListner() {
        mViewPager.setCurrentItem(2,true);
    }
}
