package com.viparking.viparking.webservice;

import com.viparking.viparking.model.webservice.WsResponse;

import org.json.JSONException;

/**
 * Created by Ferico on 7/5/16.
 */
public interface WsListener<T extends WsResponse> {
    void onSuccess(T response);
    void onError(String error);
}
