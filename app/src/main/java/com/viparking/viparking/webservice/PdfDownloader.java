package com.viparking.viparking.webservice;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.viparking.viparking.util.Util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ferico on 8/12/16.
 */
public class PdfDownloader extends AsyncTask<String, Void, Void> {

    public interface PdfDownloaderListerner{
        void onDownloadCompleted(String fileName);
    }

    private PdfDownloaderListerner callback;

    public PdfDownloaderListerner getCallback() {
        return callback;
    }

    public void setCallback(PdfDownloaderListerner callback) {
        this.callback = callback;
    }

    private String fileName;

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(callback != null)
            callback.onDownloadCompleted(fileName);
    }

    protected Void doInBackground(String... urls) {
        try {
            File dir = new File(Environment.getExternalStorageDirectory() + "/ViParking Report/");
            if(!dir.exists())
            {
                try{
                    if(dir.mkdir()) {
                        System.out.println("Directory created");
                    } else {
                        System.out.println("Directory is not created");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            URL u = new URL(urls[0]);
            InputStream is = u.openStream();

            DataInputStream dis = new DataInputStream(is);

            byte[] buffer = new byte[1024];
            int length;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            fileName = sdf.format(new Date());
            fileName += ".pdf";

            FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory()
                    + Util.FILE_PATH + fileName));
            while ((length = dis.read(buffer))>0) {
                fos.write(buffer, 0, length);
            }

        } catch (MalformedURLException mue) {
            Log.e("SYNC getUpdate", "malformed url error", mue);
        } catch (IOException ioe) {
            Log.e("SYNC getUpdate", "io error", ioe);
        } catch (SecurityException se) {
            Log.e("SYNC getUpdate", "security error", se);
        }
        return null;
    }
}
