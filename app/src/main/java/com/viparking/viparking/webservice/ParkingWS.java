package com.viparking.viparking.webservice;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.viparking.viparking.model.webservice.HistoryListResponse;
import com.viparking.viparking.model.webservice.LoginResponse;
import com.viparking.viparking.model.webservice.VehicleListResponse;
import com.viparking.viparking.model.webservice.WsResponse;
import com.viparking.viparking.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Ferico on 7/5/16.
 */
public class ParkingWS {
    private static RequestQueue sRequestQueue;

    public ParkingWS(Context context) {
        if (sRequestQueue == null)
            sRequestQueue = ParkingVolley.newRequestQueue(context);
    }

    public static void clearQueue() {

        if (sRequestQueue != null) {
            sRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
            sRequestQueue.stop();
            sRequestQueue = null;
        }
    }

    //    User Functionality=================================================================================================================
    public void login(String username, String password, WsListener callback) {

        JSONObject wrapper = new JSONObject();
        JSONObject requestObject = new JSONObject();

        try {
            requestObject.put("username", username);
            requestObject.put("password", password);

            wrapper.put("data", requestObject);
            wrapper.put("apiKey", Util.API_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //disini default passing new WsResponse(), tapi kalau ada mau dibungkus pake response model kaya yang dilakuin buat login,
        //bisa kaya yang dibawahnya
//        sRequestQueue.add(new JsonPostRequest(false, Util.LOGIN_URL, wrapper, callback, new WsResponse(),
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));

        sRequestQueue.add(new JsonPostRequest(false, Util.LOGIN_URL, wrapper, callback, new LoginResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getVehicleList(WsListener callback) {
        sRequestQueue.add(new JsonGetRequest(true, Util.MY_VEHICLE_URL, callback, new VehicleListResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getHistoryList(WsListener callback,String id){
        id = id.replace(" ","%20");
        String HistoryURL = Util.MY_HISTORY_URL.replace("{vehicleID}",id);
        sRequestQueue.add(new JsonGetRequest(true,HistoryURL,callback,new HistoryListResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void getHistoryList(WsListener callback, String id,String startDate,String endDate, int page)
    {
        id = id.replace(" ","%20");
        String HistoryURL = Util.MY_HISTORY_URL.replace("{vehicleID}",id);
        HistoryURL += "/"+page;
        if(!startDate.equals("")) {
            HistoryURL += "/" + startDate;
            HistoryURL += "/" + endDate;
        }
        sRequestQueue.add(new JsonGetRequest(true,HistoryURL,callback,new HistoryListResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void sendEmail(WsListener callback, String id,String startDate,String endDate)
    {
        id = id.replace(" ","%20");
        String SendEmailURL = Util.SEND_EMAIL_HISTORY.replace("{vehicleID}",id);
        if(!startDate.equals("")) {
            SendEmailURL += "/" + startDate;
            SendEmailURL += "/" + endDate;
        }
        sRequestQueue.add(new JsonGetRequest(true,SendEmailURL,callback,new WsResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));

    }

    public void changePassword(WsListener callback, String oldPassword, String newPassword)
    {
        JSONObject wrapper = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            data.put("oldPassword", oldPassword);
            data.put("newPassword", newPassword);

            wrapper.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sRequestQueue.add(new JsonPostRequest(true, Util.CHANGE_PASSWORD_URL, wrapper, callback, new WsResponse(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES));
    }

    public void downloadPdf(PdfDownloader.PdfDownloaderListerner callback, String id, String startDate, String endDate)
    {
        id = id.replace(" ","%20");
        String SendEmailURL = Util.DOWNLOAD_PDF_HISTORY.replace("{vehicleID}",id);
        if(!startDate.equals("")) {
            SendEmailURL += "/" + startDate;
            SendEmailURL += "/" + endDate;
        }

        PdfDownloader pdfDownloader = new PdfDownloader();
        pdfDownloader.setCallback(callback);
        pdfDownloader.execute(SendEmailURL);
        //callback.onSuccess(null);
    }
}
