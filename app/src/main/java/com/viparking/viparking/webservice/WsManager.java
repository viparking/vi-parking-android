package com.viparking.viparking.webservice;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.viparking.viparking.model.webservice.WsRequest;

/**
 * Created by Ferico on 7/5/16.
 */
public class WsManager {
    private static WsManager sInstance;

    private RequestQueue mRequestQueue;

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new WsManager(context);
        }
    }

    public static WsManager getInstance() {
        return sInstance;
    }

    private WsManager(Context context) {
        mRequestQueue = ParkingVolley.newRequestQueue(context);
    }

    public void addToQueue(WsRequest request) {
        mRequestQueue.add(request);
    }

    public void clear(Object tag) {
        if (tag != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void clearAll() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
        }
    }
}
