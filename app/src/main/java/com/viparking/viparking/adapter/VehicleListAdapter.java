package com.viparking.viparking.adapter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viparking.viparking.R;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.util.SimpleViewHolder;

import java.util.ArrayList;

/**
 * Created by Ferico on 7/19/16.
 */
public class VehicleListAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<Vehicle> vehicleList;
    private VehicleListTouchListener mCallback;

    private static int ITEM_TYPE_HEADER = 1;
    private static int ITEM_TYPE_VEHICLE = 2;

    public interface VehicleListTouchListener{
        void onTouch(Vehicle vehicle);
    }

    public VehicleListAdapter(ArrayList<Vehicle> vehicleList, VehicleListTouchListener callback){
        this.mCallback = callback;
        this.vehicleList = vehicleList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_HEADER)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_vehicle_list, parent, false);
            return new SimpleViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_item, parent, false);
        return new VehicleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == 0)
            return;
        VehicleViewHolder viewHolder = (VehicleViewHolder) holder;
        viewHolder.setVehicle(vehicleList.get(position - 1));
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return ITEM_TYPE_HEADER;
        else return ITEM_TYPE_VEHICLE;
    }

    @Override
    public int getItemCount() {
        return vehicleList.size()+1;
    }

    private class VehicleViewHolder extends ViewHolder implements View.OnClickListener {

        private TextView lblPlateNumber, lblDescription;
        private Vehicle vehicle;

        public VehicleViewHolder(View itemView){
            super(itemView);

            lblPlateNumber = (TextView) itemView.findViewById(R.id.lblPlateNumber);
            lblDescription = (TextView) itemView.findViewById(R.id.lblDescription);
            LinearLayout llVehicleListItem = (LinearLayout) itemView.findViewById(R.id.llVehicleListItem);
            llVehicleListItem.setOnClickListener(this);
        }

        private void setVehicle(Vehicle vehicle)
        {
            this.vehicle = vehicle;
            lblPlateNumber.setText(vehicle.getPlateNumber());
            lblDescription.setText(vehicle.getDescription());

        }

        @Override
        public void onClick(View v) {
            if(mCallback != null)
                mCallback.onTouch(this.vehicle);
        }
    }
}
