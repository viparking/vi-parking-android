package com.viparking.viparking.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.viparking.viparking.R;
import com.viparking.viparking.fragment.VehicleHistoryFragment;
import com.viparking.viparking.model.History;
import com.viparking.viparking.model.Vehicle;
import com.viparking.viparking.util.OnLoadMoreListener;
import com.viparking.viparking.util.SimpleViewHolder;
import com.viparking.viparking.util.Util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricky on 8/1/2016.
 */
public class HistoryListAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener
        , DatePickerDialog.OnDateSetListener {


    private static int ITEM_TYPE_HEADER = 1;
    private static int ITEM_TYPE_HISTORY = 2;
    private static int ITEM_TYPE_FOOTER = 4;
    private static int ITEM_VIEW_TYPE_LOADING = 3;
    private final int VISIBLE_THRESHOLD = 5;
    private ArrayList<History> historyList;
    private Vehicle vehicle;
    private String mStartDate="",mEndDate="";
    private int firstVisibleItem, visibleItemCount, totalItemCount, previousTotal = 0;
    private boolean loading = true;
    private Button mDownload,mSendEmail;
    private boolean endLoading = false;
    private HistoryListListener callback;
    private EditText startDate, endDate;
    private int year;
    private int month;
    private int day;
    private DatePickerDialog startDateDialog;
    private DatePickerDialog endDateDialog;
    public HistoryListAdapter(ArrayList<History> historyList){
        this.historyList = historyList;
    }

    public void setVehicle(Vehicle vehicle){
        this.vehicle = vehicle;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.startDate:
                startDateDialog.show();
                break;
            case R.id.endDate:
                endDateDialog.show();
                break;
            case R.id.btnSendEmail:
                callback.sendEmail(mStartDate,mEndDate);
                break;
            case R.id.btnDownloadPDF:
                callback.downloadPdf(mStartDate,mEndDate);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
        year = selectedYear;
        month = selectedMonth;
        day = selectedDay;

        // set selected date into datepicker also
        view.init(year, month, day, null);
    }

    public interface HistoryListListener
    {
        void shouldLoadNextPage();
        void setCalendar(String starDate,String endDate);
        void sendEmail(String starDate,String endDate);
        void downloadPdf(String startDate, String endDate);
    }

    public HistoryListAdapter (RecyclerView recyclerView, ArrayList<History> dataSet, HistoryListListener callback) {
        this.historyList = dataSet;
        this.callback = callback;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                    totalItemCount = linearLayoutManager.getItemCount();
//                    visibleItemCount = linearLayoutManager.getChildCount();
//                    firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
//
//                    if (loading && !endLoading) {
//                        if (totalItemCount > previousTotal) {
//                            loading = false;
//                            previousTotal = totalItemCount;
//                        }
//                    }
//                    if (!loading && (totalItemCount - visibleItemCount)
//                            <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
//                        // End has been reached
//
//                        if (onLoadMoreListener != null) {
//                            onLoadMoreListener.onLoadMore();
//                        }
//                        loading = true;
//                    }
//                }
//            });
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_TYPE_HEADER)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_history_list, parent, false);
            TextView lblPlateNumber,lblBalance;
            lblPlateNumber = (TextView) view.findViewById(R.id.lblPlateNumber);
            lblBalance = (TextView) view.findViewById(R.id.lblBalance);
            if(vehicle != null) {
                lblBalance.setText(Util.formatMoney(vehicle.getBalance()));
                lblPlateNumber.setText(vehicle.getPlateNumber());
            }
            else {
                lblBalance.setText(Util.formatMoney(0));
                lblPlateNumber.setText("");
            }
            startDate = (EditText) view.findViewById(R.id.startDate);
            endDate = (EditText) view.findViewById(R.id.endDate);
            startDate.setRawInputType(InputType.TYPE_NULL);
            endDate.setRawInputType(InputType.TYPE_NULL);


            startDate.setOnClickListener(this);
            endDate.setOnClickListener(this);
            if(!mStartDate.equals("")){
                startDate.setText(mStartDate);
            }

            if(!mEndDate.equals("")){
                endDate.setText(mEndDate);
            }

            Calendar newCalendar = Calendar.getInstance();
            startDateDialog = new DatePickerDialog((VehicleHistoryFragment.getInstance().getContext()), new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar start = Calendar.getInstance();
                    if(monthOfYear == 0){
                        monthOfYear = 1;
                    }else {
                        ++monthOfYear;
                    }

                    start.set(year, monthOfYear, dayOfMonth);
                    mStartDate = String.valueOf(dayOfMonth) + "-" +
                            String.valueOf(monthOfYear)+ "-" +
                            String.valueOf(year);
                    startDate.setText(mStartDate);



                    if(!(endDate.getText().toString().equals("") || startDate.getText().toString().equals(""))){
                        callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                    }


                    if(endDate.getText().toString().equals("") && startDate.getText().toString().equals("")){
                        callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            //startDateDialog.


            endDateDialog = new DatePickerDialog((VehicleHistoryFragment.getInstance().getContext()), new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar end = Calendar.getInstance();
                    if(monthOfYear == 0){
                        monthOfYear = 1;
                    }else {
                        ++monthOfYear;
                    }
                    end.set(year, monthOfYear, dayOfMonth);
                    mEndDate = String.valueOf(dayOfMonth) + "-" +
                            String.valueOf(monthOfYear)+ "-" +
                            String.valueOf(year);
                    endDate.setText(mEndDate);


                    if(!(endDate.getText().toString().equals("") || startDate.getText().toString().equals(""))){
                        callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                    }

                    if(endDate.getText().toString().equals("") && startDate.getText().toString().equals("")){
                        callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                    }


                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            endDateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        dialog.dismiss();
                        endDate.setText("");
                        if(endDate.getText().toString().equals("") && startDate.getText().toString().equals("")){
                            callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                        }

                    }
                }
            });

            startDateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        dialog.dismiss();
                        startDate.setText("");
                        if(endDate.getText().toString().equals("") && startDate.getText().toString().equals("")){
                            callback.setCalendar(startDate.getText().toString(),endDate.getText().toString());
                        }

                    }
                }
            });
            return new SimpleViewHolder(view);
        }else if(viewType == ITEM_TYPE_FOOTER){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_history_list,parent,false);
            mDownload = (Button)view.findViewById(R.id.btnDownloadPDF);
            mSendEmail = (Button)view.findViewById(R.id.btnSendEmail);
            mDownload.setOnClickListener(this);
            mSendEmail.setOnClickListener(this);
            return new SimpleViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_item, parent, false);

        return new HistoryViewHolder(view);
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {

        private TextView lblLocation, lblDuration,lblTime,lblPrice;
        private History history;

        public HistoryViewHolder(View itemView){
            super(itemView);
            lblLocation = (TextView) itemView.findViewById(R.id.lblLocation);
            lblDuration = (TextView) itemView.findViewById(R.id.lblDuration);
            lblTime = (TextView)itemView.findViewById(R.id.lblTime);
            lblPrice = (TextView)itemView.findViewById(R.id.lblPrice);
        }

        private void setHistory(History history) throws ParseException {
            this.history = history;
            lblLocation.setText(history.getParkingLotName());
            if(history.getOutTime().equals("0000-00-00 00:00:00")){
                history.setOutTime("2016-12-12 12:12:12");
            }
            lblDuration.setText(Util.CountDuration(history.getInTime(),history.getOutTime()));
            lblPrice.setText(Util.formatMoney(history.getPrice()));
            lblTime.setText(history.getInTime()+" - "+history.getOutTime());
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == 0 || getItemViewType(position) == ITEM_TYPE_FOOTER)
            return;
        HistoryViewHolder viewHolder = (HistoryViewHolder) holder;
        try {
            viewHolder.setHistory(historyList.get(position - 1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(position == historyList.size())
            callback.shouldLoadNextPage();
    }


    @Override
    public int getItemCount() {
        return historyList.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return ITEM_TYPE_HEADER;
        else if(position == historyList.size()+1){
            return ITEM_TYPE_FOOTER;
        }
        else return ITEM_TYPE_HISTORY;
    }

    public ArrayList<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(ArrayList<History> historyList) {
        this.historyList = historyList;
    }
}
