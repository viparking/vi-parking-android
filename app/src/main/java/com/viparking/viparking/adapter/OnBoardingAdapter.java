package com.viparking.viparking.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viparking.viparking.R;
import com.viparking.viparking.fragment.OnBoardingFrament;

/**
 * Created by Ferico on 7/12/16.
 */
public class OnBoardingAdapter extends FragmentPagerAdapter {

    private static final int[] IMAGE_RESOURCE = {
            R.drawable.img_onboarding_1,
            R.drawable.img_onboarding_2,
            R.drawable.img_onboarding_3,
            R.drawable.img_onboarding_4,
    };

    private static final int[] TITLE_RESOURCE = {
            R.string.on_boarding_title_1,
            R.string.on_boarding_title_2,
            R.string.on_boarding_title_3,
            R.string.on_boarding_title_4
    };

    private static final int[] DESCRIPTION_RESOUCE = {
            R.string.on_boarding_description_1,
            R.string.on_boarding_description_2,
            R.string.on_boarding_description_3,
            R.string.on_boarding_description_4
    };

    public OnBoardingAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return OnBoardingFrament.newInstance(IMAGE_RESOURCE[position], TITLE_RESOURCE[position], DESCRIPTION_RESOUCE[position]);
    }

    @Override
    public int getCount() {
        return 4;
    }
}
