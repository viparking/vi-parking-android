package com.viparking.viparking.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.viparking.viparking.R;
import com.viparking.viparking.fragment.CheckSaldoFragment;
import com.viparking.viparking.fragment.SettingFragment;
import com.viparking.viparking.fragment.VehicleHistoryFragment;
import com.viparking.viparking.fragment.VehicleListFragment;
import com.viparking.viparking.model.Vehicle;

import java.util.ArrayList;

/**
 * Created by Ferico on 7/17/16.
 */
public class DashboardPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private VehicleListFragment.VehicleListListener callback;
    private CheckSaldoFragment.ViewHistoryListner HistoryCallback;
    public DashboardPagerAdapter(FragmentManager fm, Context context, VehicleListFragment.VehicleListListener callback,CheckSaldoFragment.ViewHistoryListner HistoryCallback)
    {
        super(fm);
        this.callback = callback;
        this.context = context;
        this.HistoryCallback = HistoryCallback;
    }

    public void setVehicle(Vehicle vehicle)
    {
        ((CheckSaldoFragment)getItem(0)).setVehicle(vehicle);

    }

    public void setHistoryList(Vehicle vehicle){
        ((VehicleHistoryFragment)getItem(2)).setVehicle(vehicle);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position)
        {
            case 0:
                fragment = CheckSaldoFragment.getInstance(HistoryCallback); //singleton
                break;
            case 1:
                fragment = VehicleListFragment.getInstance(callback);
                break;
            case 2:
                fragment = VehicleHistoryFragment.getInstance();
                break;
            case 3:
                fragment = SettingFragment.getInstance();
                break;

        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return context.getString(R.string.tab_cek_saldo);
            case 1:
                return context.getString(R.string.tab_change_vehicle);
            case 2:
                return context.getString(R.string.tab_history);
            case 3:
                return context.getString(R.string.tab_setting);
            default:
                return "";
        }
    }
}
