package com.viparking.viparking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.viparking.viparking.model.User;
import com.viparking.viparking.model.webservice.LoginResponse;
import com.viparking.viparking.util.Util;
import com.viparking.viparking.webservice.ParkingWS;
import com.viparking.viparking.webservice.WsListener;

public class LoginActivity extends AppCompatActivity implements WsListener<LoginResponse>, View.OnClickListener {

    private EditText txtUsername, txtPassword;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onSuccess(LoginResponse response) {
        mProgressDialog.dismiss();
        if(response.isMember())
        {
            User user = response.getUser();
            if(user.isBanned())
            {
                Snackbar snackbar = Snackbar.make(txtPassword, getString(R.string.error_banned), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            Util.SET_ACCESS_TOKEN(response.getAccessToken());
            Util.SET_USER_ID(user.getId());
            Util.SET_USER_NAME(user.getUsername());

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            Snackbar snackbar = Snackbar.make(txtPassword, getString(R.string.error_not_member), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onError(String error) {
        mProgressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(txtPassword, error, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        if(validateInput()) {
            ParkingWS ws = new ParkingWS(LoginActivity.this);
            mProgressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.loading), getString(R.string.please_wait), true, false);
            ws.login(txtUsername.getText().toString(), txtPassword.getText().toString(), LoginActivity.this);
        }
    }

    private boolean validateInput()
    {
        if(txtPassword.getText().length() == 0)
        {
            showErrorMessage(getString(R.string.error_empty_password));
            txtPassword.requestFocus();
            return false;
        }
        else if(txtUsername.getText().length() == 0)
        {
            showErrorMessage(getString(R.string.error_empty_username));
            txtUsername.requestFocus();
            return false;
        }
        return true;
    }

    private void showErrorMessage(String error)
    {
        Snackbar snackbar = Snackbar.make(txtPassword, error, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
